use bracket_lib::prelude::*;


const SCREEN_WIDTH: i32 = 80;
const SCREEN_HEIGHT: i32 = 50;
const FRAME_DURATION: f32 = 8.0;
const VELOCITY_INCREMENT: f32 = 0.1;
enum GameMode {
  Menu,
  Playing,
  End,
}

struct State {
  mode: GameMode,
  player: Player,
  frame_time: f32,
  wall: Wall,
  score: i32,
}

struct Player {
  x: i32,
  y: f32,
  v: f32,
}

struct Wall {
  x: i32,
  y: i32,
  gap: i32,
}

impl Wall {

  fn new(x: i32, score: i32) -> Wall {
    let mut random = RandomNumberGenerator::new();
    Wall {
      x,
      y: random.range(10,40),
      gap: i32::max(2,20-score),
    }
  }

  fn is_hit(&self, player: &Player) -> bool {
    let half_size = self.gap / 2;
    let x_intersect = player.x == self.x;
    let player_in_gap = ((player.y.round() as i32) < (self.y + half_size)) && ((player.y.round() as i32) > (self.y - half_size));
    !player_in_gap && x_intersect
  }

  fn render(&self, ctx: &mut BTerm, player_x: i32) {
    let screen_x = self.x - player_x + 1;
    let half_size = self.gap / 2;

    for y in 0..self.y - half_size {
      ctx.set(
        screen_x,
        y,
        GREEN,
        GREEN,
        to_cp437('|'),
      );
    }

    for y in self.y + half_size..SCREEN_HEIGHT {
      ctx.set(
        screen_x,
        y,
        GREEN,
        GREEN,
        to_cp437('|'),
      );
    }
  }
}

impl GameState for State {
  fn tick(&mut self, ctx: &mut BTerm) {
    match self.mode {
      GameMode::Menu => self.main_menu(ctx),
      GameMode::Playing => self.play(ctx),
      GameMode::End => self.dead(ctx),
    }
  }
}

impl State {
  fn new() -> Self {
    State {
      mode: GameMode::Menu,
      player: Player {x: 1, y: (SCREEN_HEIGHT/2) as f32, v: -1.0},
      frame_time: 0.0,
      wall: Wall::new(SCREEN_WIDTH,0),
      score: 0,
    }
  }

  fn main_menu(&mut self, ctx: &mut BTerm) {
    ctx.print(1, 1, "Henlo frens");
    ctx.print(5, 5, "Welcome to the thing");
    ctx.print(5, 8, "(P) Play plox");
    ctx.print(5, 9, "(Q) Quit with fire");
    if let Some(key) = ctx.key {
      match key {
        VirtualKeyCode::P => self.restart(),
        VirtualKeyCode::Q => ctx.quitting = true,
        _ => {},
      }
    }
  }
  
  fn play(&mut self, ctx: &mut BTerm) {
    ctx.cls_bg(BLACK);
    ctx.print(5, 2, format!("Score: {}", self.score));
    self.frame_time += ctx.frame_time_ms;
    if self.frame_time > FRAME_DURATION {
      self.player.gravity_and_move();
      self.wall.render(ctx, self.player.x);
      if self.wall.is_hit(&self.player) {
        self.mode = GameMode::End;
      }
      if self.player.x > self.wall.x {
        self.score += 1;
        self.wall = Wall::new(self.player.x + SCREEN_WIDTH, self.score);
      }
      self.frame_time = 0.0;
    };
    ctx.print(1, self.player.y.round() as i32, "@");

    if let Some(VirtualKeyCode::Space) = ctx.key {
      self.player.v = f32::min(-1.0,-VELOCITY_INCREMENT);
    }

    if self.player.y > SCREEN_HEIGHT as f32 {
      self.mode = GameMode::End;
    }
  }

  fn dead(&mut self, ctx: &mut BTerm) {
    ctx.cls_bg(RED);
    ctx.print(5, 2, format!("Score: {}", self.score));
    ctx.print(5,5,"You are kill");
    if let Some(key) = ctx.key {
      match key {
        VirtualKeyCode::P => self.restart(),
        VirtualKeyCode::Q => ctx.quitting = true,
        _ => {},
      }
    }
  }

  fn restart(&mut self) {
      self.mode = GameMode::Playing;
      self.player = Player {x: 1, y: (SCREEN_HEIGHT/2) as f32, v: -1.0};
      self.frame_time = 0.0;
      self.wall = Wall::new(SCREEN_WIDTH,0);
      self.score = 0;
  }
}

impl Player {
  fn gravity_and_move(&mut self) {
    self.y += self.v;
    self.v += VELOCITY_INCREMENT;
    self.x += 1;
  }
}


fn main() -> BError {
  let context = BTermBuilder::simple80x50()
    .with_title("henlo pls")
    .build()?;
  main_loop(context, State::new())
}
